<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RUANG BACA</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/nucleo-icons.css" rel="stylesheet">

</head>

<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-transparent nav-down" color-on-scroll="500">
        <div class="container">
            <div class="navbar-translate">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">RUANG BACA</a>
                </div>
                <button class="navbar-toggler navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="team" data-scroll="true" href="javascript:void(0)">Team</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">Sections</a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-danger">
                            <a class="dropdown-item" href="sections.html#headers"><i class="nc-icon nc-tile-56"></i>&nbsp; Headers</a>
                            <a class="dropdown-item" href="sections.html#features"><i class="nc-icon nc-settings"></i>&nbsp; Features</a>
                            <a class="dropdown-item" href="sections.html#blogs"><i class="nc-icon nc-bullet-list-67"></i>&nbsp; Blogs</a>
                            <a class="dropdown-item" href="sections.html#teams"><i class="nc-icon nc-single-02"></i>&nbsp; Teams</a>
                            <a class="dropdown-item" href="sections.html#projects"><i class="nc-icon nc-calendar-60"></i>&nbsp; Projects</a>
                            <a class="dropdown-item" href="sections.html#pricing"><i class="nc-icon nc-money-coins"></i>&nbsp; Pricing</a>
                            <a class="dropdown-item" href="sections.html#testimonials"><i class="nc-icon nc-badge"></i>&nbsp; Testimonials</a>
                            <a class="dropdown-item" href="sections.html#contact-us"><i class="nc-icon nc-mobile"></i>&nbsp; Contacts</a>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"  data-toggle="dropdown" href="javascript:void(0)">Examples</a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-danger">
                            <a class="dropdown-item" href="examples/about-us.html"><i class="nc-icon nc-bank"></i>&nbsp; About Us</a>
                            <a class="dropdown-item" href="examples/add-product.html"><i class="nc-icon nc-basket"></i>&nbsp; Add Product</a>
                            <a class="dropdown-item" href="examples/blog-post.html"><i class="nc-icon nc-badge"></i>&nbsp; Blog Post</a>
                            <a class="dropdown-item" href="examples/blog-posts.html"><i class="nc-icon nc-bullet-list-67"></i>&nbsp; Blog Posts</a>
                            <a class="dropdown-item" href="examples/contact-us.html"><i class="nc-icon nc-mobile"></i>&nbsp; Contact Us</a>
                            <a class="dropdown-item" href="examples/discover.html"><i class="nc-icon nc-world-2"></i>&nbsp; Discover</a>
                            <a class="dropdown-item" href="examples/ecommerce.html"><i class="nc-icon nc-send"></i>&nbsp; Ecommerce</a>
                            <a class="dropdown-item" href="examples/landing.html"><i class="nc-icon nc-spaceship"></i>&nbsp; Landing</a>
                            <a class="dropdown-item" href="examples/login.html"><i class="nc-icon nc-lock-circle-open"></i>&nbsp; Login</a>
                            <a class="dropdown-item" href="examples/product-page.html"><i class="nc-icon nc-album-2"></i>&nbsp; Product Page</a>
                            <a class="dropdown-item" href="examples/profile.html"><i class="nc-icon nc-single-02"></i>&nbsp; Profile</a>
                            <a class="dropdown-item" href="examples/register.html"><i class="nc-icon nc-bookmark-2"></i>&nbsp; Register</a>
                            <a class="dropdown-item" href="examples/search-with-sidebar.html"><i class="nc-icon nc-zoom-split"></i>&nbsp; Search</a>
                            <a class="dropdown-item" href="examples/settings.html"><i class="nc-icon nc-settings-gear-65"></i>&nbsp; Settings</a>
                            <a class="dropdown-item" href="examples/twitter-redesign.html"><i class="nc-icon nc-tie-bow"></i>&nbsp; Twitter</a>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-round btn-danger" href="/register">
                            <i class="fa fa-id-card-o" aria-hidden="true"></i> register
                        </a>
                    </li>
                     <li class="nav-item">
                        <a class="btn btn-round btn-danger" href="/login">
                            <i class="fa fa-unlock-alt" aria-hidden="true"></i> lOGIN
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper">
        <div class="page-header" style="background-image: url('assets/img/692579.jpg')">
            <div class="filter"></div>
            <div class="content-center">
                <div class="motto">
                    <h1 class="text-center">RUANG BACA</h1>
                    <h3 class="text-center">SMKN 1 SURABAYA</h3>
                </div>
            </div>
        </div>
        
        
    <div class="section section-gray" id="cards">
            <div class="container tim-container">
                <div class="title">
                    <h2>Populer</h2>
                </div>
                <h4 class="title">Artikel</h4>

                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="card card-blog">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="img" src="assets/img/sections/neill-kumar.jpg">
                                </a>
                            </div>
                            <div class="card-body text-center">
                                <h4 class="card-title">
                                    We will create a great experience
                                </h4>
                                <div class="card-description">
                                    Cards are an interaction model that are spreading pretty widely, in fact. What all of these have in common is that they're pulling...
                                </div>
                                <div class="card-footer">
                                    <a href="#pablo" class="btn btn-danger btn-round">View Article</a>
                                </div>
                            </div>
                        </div>

                        <div class="card" data-color="purple" data-background="color">
                            <div class="card-body text-center">
                                <h6 class="card-category">
                                    <i class="fa fa-dribbble" aria-hidden="true"></i> Dribbble
                                </h6>
                                <h5 class="card-title">
                                    <a href="#pablo">"Good Design Is as Little Design as Possible"</a>
                                </h5>
                                <p class="card-description">
                                    Design makes an important contribution to the preservation of the environment. It conserves resources and minimises physical and visual pollution throughout the lifecycle of the product....
                                </p>
                                <div class="card-footer text-center">
                                    <a href="#pablo" rel="tooltip" title="Bookmark" class="btn btn-outline-neutral btn-round btn-just-icon"><i class="fa fa-bookmark-o"></i></a>
                                    <a href="#pablo" class="btn btn-neutral btn-round"><i class="fa fa-newspaper-o"></i> Read</a>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <h6 class="author pull-left">Ronna Johnson</h6>
                                <span class="category-social text-info pull-right">
                                    <i class="fa fa-twitter"></i>
                                </span>
                                <div class="clearfix"></div>
                                <p class="card-description">
                                    "It clarifies the product’s structure. Better still, it can make the product clearly express its function by making use of the <a href="#twitter" class="text-danger">@mike</a>'s intuition. At best, it is self-explanatory."
                                </p>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="card card-blog">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="img" src="assets/img/sections/anders-jilden.jpg">
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">
                                    Good Design Is Aesthetic
                                </h4>
                                <div class="card-description">
                                    Cards are an interaction model that are spreading pretty widely, in fact. What all of these have in common is that they're pulling...
                                </div>
                                <hr />
                                <div class="card-footer">
                                    <div class="author">
                                        <a href="#pablo">
                                           <img src="assets/img/faces/ayo-ogunseinde-2.jpg" alt="..." class="avatar img-raised">
                                           <span>Mike John</span>
                                        </a>
                                    </div>
                                    <div class="stats">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i> 5 min read
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card" data-background="image" style="background-image: url('assets/img/sections/rawpixel-com.jpg')">
                            <div class="card-body">
                                <a href="#pablo">
                                    <h3 class="card-title">Ten Principles of “Good Design”</h3>
                                </a>
                                <p class="card-description">
                                    Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is...
                                </p>
                                <div class="card-footer">
                                    <a href="#pablo" class="btn btn-success btn-round">
                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Download PDF
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <div class="card" data-background="color" data-color="orange">
                            <div class="card-body">
                                <div class="author">
                                    <a href="#pablo">
                                       <img src="assets/img/faces/erik-lucatero-2.jpg" alt="..." class="avatar img-raised">
                                       <span>Erik Johnson</span>
                                    </a>
                                </div>
                                <span class="category-social pull-right">
                                    <i class="fa fa-quote-right"></i>
                                </span>
                                <div class="clearfix"></div>
                                <p class="card-description">
                                    "Less, but better – because it concentrates on the essential aspects, and the products are not burdened with non-essentials. Back to purity, back to simplicity. At best, it is self-explanatory."
                                </p>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body text-center">
                                <span class="category-social text-info pull-right">
                                    <i class="fa fa-twitter"></i>
                                </span>
                                <div class="clearfix"></div>
                                <div class="author">
                                    <a href="#pablo">
                                       <img src="assets/img/faces/kaci-baum-2.jpg" alt="..." class="avatar-big img-raised border-gray">
                                    </a>
                                    <h5 class="card-title">Kaci Baum</h5>
                                    <p class="category"><a href="#twitter" class="text-danger">@kacibaum</a></p>
                                </div>
                                <p class="card-description">
                                    "Less, but better – because it concentrates on the essential aspects, and the products are not burdened with non-essentials."
                                </p>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-image">
                                <a href="#pablo">
                                    <img class="img" src="assets/img/sections/david-marcu.jpg">
                                </a>
                            </div>
                            <div class="card-body">
                                <label class="label label-warning">Travel</label>
                                <h5 class="card-title">
                                    Currently at the San Francisco Museum of Modern Art
                                </h5>
                                <hr />
                                <div class="card-footer">
                                    <div class="author">
                                        <a href="#pablo">
                                           <img src="assets/img/rihanna.jpg" alt="..." class="avatar img-raised">
                                           <span>Lord Alex</span>
                                        </a>
                                    </div>
                                   <div class="stats">
                                        <i class="fa fa-heart" aria-hidden="true"></i> 5.3k
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
               
                </div>
            </div>
    </div>
</div>
    <footer class="footer section-dark">
        <div class="container">
            <div class="row">
                <nav class="footer-nav">
                    <ul>
                       
                    </ul>
                </nav>
                <div class="credits ml-auto">
                    <span class="copyright">
                        © <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by Rizky putra
                    </span>
                </div>
            </div>
        </div>
    </footer>
</body>

<!-- Core JS Files -->
<script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
<script src="assets/js/popper.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Switches -->
<script src="assets/js/bootstrap-switch.min.js"></script>
<!--  Plugins for Slider -->
<script src="assets/js/nouislider.js"></script>

<!--  Photoswipe files -->
<script src="assets/js/photo_swipe/photoswipe.min.js"></script>
<script src="assets/js/photo_swipe/photoswipe-ui-default.min.js"></script>
<script src="assets/js/photo_swipe/init-gallery.js"></script>

<!--  Plugins for Select -->
<script src="assets/js/bootstrap-select.js"></script>

<!--  for fileupload -->
<script src="assets/js/jasny-bootstrap.min.js"></script>

<!--  Plugins for Tags -->
<script src="assets/js/bootstrap-tagsinput.js"></script>

<!--  Plugins for DateTimePicker -->
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>

<script src="assets/js/paper-kit.js?v=2.1.0"></script>
</html>