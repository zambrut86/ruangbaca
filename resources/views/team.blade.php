<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>RUANG BACA</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/paper-kit.css?v=2.1.0" rel="stylesheet"/>
    <link href="assets/css/demo.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/nucleo-icons.css" rel="stylesheet">

</head>

<body>
    <nav class="navbar navbar-expand-lg fixed-top nav-down">
        <div class="container">
            <div class="navbar-translate">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">RUANG BACA</a>
                </div>
                <button class="navbar-toggler navbar-burger" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                    <span class="navbar-toggler-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="team" data-scroll="true" href="javascript:void(0)">TEAM</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">Sections</a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-danger">
                            <a class="dropdown-item" href="sections.html#headers"><i class="nc-icon nc-tile-56"></i>&nbsp; Headers</a>
                            <a class="dropdown-item" href="sections.html#features"><i class="nc-icon nc-settings"></i>&nbsp; Features</a>
                            <a class="dropdown-item" href="sections.html#blogs"><i class="nc-icon nc-bullet-list-67"></i>&nbsp; Blogs</a>
                            <a class="dropdown-item" href="sections.html#teams"><i class="nc-icon nc-single-02"></i>&nbsp; Teams</a>
                            <a class="dropdown-item" href="sections.html#projects"><i class="nc-icon nc-calendar-60"></i>&nbsp; Projects</a>
                            <a class="dropdown-item" href="sections.html#pricing"><i class="nc-icon nc-money-coins"></i>&nbsp; Pricing</a>
                            <a class="dropdown-item" href="sections.html#testimonials"><i class="nc-icon nc-badge"></i>&nbsp; Testimonials</a>
                            <a class="dropdown-item" href="sections.html#contact-us"><i class="nc-icon nc-mobile"></i>&nbsp; Contacts</a>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle"  data-toggle="dropdown" href="javascript:void(0)">Examples</a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-danger">
                            <a class="dropdown-item" href="examples/about-us.html"><i class="nc-icon nc-bank"></i>&nbsp; About Us</a>
                            <a class="dropdown-item" href="examples/add-product.html"><i class="nc-icon nc-basket"></i>&nbsp; Add Product</a>
                            <a class="dropdown-item" href="examples/blog-post.html"><i class="nc-icon nc-badge"></i>&nbsp; Blog Post</a>
                            <a class="dropdown-item" href="examples/blog-posts.html"><i class="nc-icon nc-bullet-list-67"></i>&nbsp; Blog Posts</a>
                            <a class="dropdown-item" href="examples/contact-us.html"><i class="nc-icon nc-mobile"></i>&nbsp; Contact Us</a>
                            <a class="dropdown-item" href="examples/discover.html"><i class="nc-icon nc-world-2"></i>&nbsp; Discover</a>
                            <a class="dropdown-item" href="examples/ecommerce.html"><i class="nc-icon nc-send"></i>&nbsp; Ecommerce</a>
                            <a class="dropdown-item" href="examples/landing.html"><i class="nc-icon nc-spaceship"></i>&nbsp; Landing</a>
                            <a class="dropdown-item" href="examples/login.html"><i class="nc-icon nc-lock-circle-open"></i>&nbsp; Login</a>
                            <a class="dropdown-item" href="examples/product-page.html"><i class="nc-icon nc-album-2"></i>&nbsp; Product Page</a>
                            <a class="dropdown-item" href="examples/profile.html"><i class="nc-icon nc-single-02"></i>&nbsp; Profile</a>
                            <a class="dropdown-item" href="examples/register.html"><i class="nc-icon nc-bookmark-2"></i>&nbsp; Register</a>
                            <a class="dropdown-item" href="examples/search-with-sidebar.html"><i class="nc-icon nc-zoom-split"></i>&nbsp; Search</a>
                            <a class="dropdown-item" href="examples/settings.html"><i class="nc-icon nc-settings-gear-65"></i>&nbsp; Settings</a>
                            <a class="dropdown-item" href="examples/twitter-redesign.html"><i class="nc-icon nc-tie-bow"></i>&nbsp; Twitter</a>
                        </ul>
                    </li>
                   <li class="nav-item">
                        <a class="btn btn-round btn-danger" href="/register">
                            <i class="fa fa-id-card-o" aria-hidden="true"></i> register
                        </a>
                    </li>
                     <li class="nav-item">
                        <a class="btn btn-round btn-danger" href="/login">
                            <i class="fa fa-unlock-alt" aria-hidden="true"></i> lOGIN
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    
        
    <div class="section section-gray" id="cards">
            <div class="container tim-container">
                

                <h4 class="title">Team Members Cards</h4>
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="card card-profile">
                            <div class="card-cover" style="background-image: url('assets/img/sections/pavel-kosov.jpg')">
                            </div>
                            <div class="card-avatar border-white">
                                <a href="#avatar">
                                    <img src="assets/img/faces/joe-gardner-2.jpg" alt="..."/>
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Audrey Hepburn</h4>
                                <h6 class="card-category">Developer</h6>

                                <div class="card-footer text-center">
                                    <a href="#paper-kit" class="btn btn-just-icon btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-dribbble">
                                        <i class="fa fa-dribbble"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-facebook">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </div>
                            </div>
                        </div> <!-- end card -->
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="card card-profile">
                            <div class="card-cover" style="background-image: url('assets/img/sections/pavel-kosov.jpg')">
                            </div>
                            <div class="card-avatar border-white">
                                <a href="#avatar">
                                    <img src="assets/img/faces/joe-gardner-2.jpg" alt="..."/>
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Audrey Hepburn</h4>
                                <h6 class="card-category">Developer</h6>

                                <div class="card-footer text-center">
                                    <a href="#paper-kit" class="btn btn-just-icon btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-dribbble">
                                        <i class="fa fa-dribbble"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-facebook">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </div>
                            </div>
                        </div> <!-- end card -->
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="card card-profile">
                            <div class="card-cover" style="background-image: url('assets/img/sections/pavel-kosov.jpg')">
                            </div>
                            <div class="card-avatar border-white">
                                <a href="#avatar">
                                    <img src="assets/img/faces/joe-gardner-2.jpg" alt="..."/>
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Audrey Hepburn</h4>
                                <h6 class="card-category">Developer</h6>

                                <div class="card-footer text-center">
                                    <a href="#paper-kit" class="btn btn-just-icon btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-dribbble">
                                        <i class="fa fa-dribbble"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-facebook">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </div>
                            </div>
                        </div> <!-- end card -->
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="card card-profile">
                            <div class="card-cover" style="background-image: url('assets/img/sections/pavel-kosov.jpg')">
                            </div>
                            <div class="card-avatar border-white">
                                <a href="#avatar">
                                    <img src="assets/img/faces/joe-gardner-2.jpg" alt="..."/>
                                </a>
                            </div>
                            <div class="card-body">
                                <h4 class="card-title">Audrey Hepburn</h4>
                                <h6 class="card-category">Developer</h6>

                                <div class="card-footer text-center">
                                    <a href="#paper-kit" class="btn btn-just-icon btn-twitter">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-dribbble">
                                        <i class="fa fa-dribbble"></i>
                                    </a>
                                    <a href="#paper-kit" class="btn btn-just-icon btn-facebook">
                                        <i class="fa fa-facebook-square"></i>
                                    </a>
                                </div>
                            </div>
                        </div> <!-- end card -->

                    </div>

                </div>

               
                </div>
            </div>
    </div>
</div>
    <footer class="footer section-dark">
        <div class="container">
            <div class="row">
                <nav class="footer-nav">
                    <ul>
                       
                    </ul>
                </nav>
                <div class="credits ml-auto">
                    <span class="copyright">
                        © <script>document.write(new Date().getFullYear())</script>, made with <i class="fa fa-heart heart"></i> by Rizky putra
                    </span>
                </div>
            </div>
        </div>
    </footer>
</body>

<!-- Core JS Files -->
<script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui-1.12.1.custom.min.js" type="text/javascript"></script>
<script src="assets/js/popper.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!-- Switches -->
<script src="assets/js/bootstrap-switch.min.js"></script>
<!--  Plugins for Slider -->
<script src="assets/js/nouislider.js"></script>

<!--  Photoswipe files -->
<script src="assets/js/photo_swipe/photoswipe.min.js"></script>
<script src="assets/js/photo_swipe/photoswipe-ui-default.min.js"></script>
<script src="assets/js/photo_swipe/init-gallery.js"></script>

<!--  Plugins for Select -->
<script src="assets/js/bootstrap-select.js"></script>

<!--  for fileupload -->
<script src="assets/js/jasny-bootstrap.min.js"></script>

<!--  Plugins for Tags -->
<script src="assets/js/bootstrap-tagsinput.js"></script>

<!--  Plugins for DateTimePicker -->
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>

<script src="assets/js/paper-kit.js?v=2.1.0"></script>
</html>